<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">


                <router-link exact-active-class="active" :to="{ path: '/'}" :class="['nav-link']">
                    <i class="icon-speedometer"></i>Dashboard
                </router-link>

            </li>
            <li class="nav-title">
                Mantenimiento
            </li>
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'category'}" :class="['nav-link']">
                    <i class="icon-info"></i>Categorias<span class="badge badge-info">IT</span>
                </router-link>
            </li>
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'criteria'}" :class="['nav-link']">
                    <i class="icon-info"></i>Criterios<span class="badge badge-info">IT</span>
                </router-link>
            </li>


        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>