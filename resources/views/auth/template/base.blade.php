<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="noindex, nofollow">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ secure_asset('js/template.js') }}" defer></script>

  <!-- Styles -->
  <link href="{{  secure_asset('css/template.css') }}" rel="stylesheet">
</head>

<body class="app flex-row align-items-center">
  <div class="container">
    @yield('content')
  </div>
</body>

</html>