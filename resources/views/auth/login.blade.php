@extends('auth.template.base')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card-group mb-0">
            <div class="card p-4">
                <div class="card-body">
                    <h1 class="text-center">{{ config('app.name', 'Laravel') }}</h1>
                    <p class="text-muted text-center">Control de acceso al sistema</p>
                    <form method="POST" action="{{ secure_url('login')}}">
                        @csrf
                        <div>
                            <div class="input-group mb-3">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}"
                                    required autocomplete="email" autofocus>
                            </div>
                            @error('email')
                            <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div>
                            <div class="input-group mb-4">
                                <span class="input-group-addon"><i class="icon-lock"></i></span>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required placeholder="{{ __('Password') }}" autocomplete="current-password">
                            </div>

                            @error('password')
                            <span class="invalid-feedback" role="alert" style="display:block;">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col text-center">
                                <button type="submit" class="btn btn-primary px-4">{{ __('Login') }}</button>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection