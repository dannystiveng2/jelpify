@extends('main')
@section('content')
<main class="main">
    <div class="container-fluid">
        <router-view></router-view>
    </div>
</main>

@endsection