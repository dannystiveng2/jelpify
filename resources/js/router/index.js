
import Home from "@/components/ExampleComponent.vue";
import Category from "@/components/Category/CategoryComponent.vue";
import Criteria from "@/components/Criteria/CriteriaComponent.vue";


export default [
    { path: '', name: 'index', component: Home },
    { path: '/category', name: 'category', component: Category },
    { path: '/criteria', name: 'criteria', component: Criteria },
    { path: '**', name: 'todo', component: Home },



];