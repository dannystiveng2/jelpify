<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/admin')->name('admin.')->namespace('Admin')
        //->middleware(['auth', 'role:superadmin|admin'])
        ->group(function () {
            Route::get('/', function () {
                return view('content.content');
            })->name('main');
        });

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
